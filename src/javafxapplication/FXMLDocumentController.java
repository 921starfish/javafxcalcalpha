/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication;

import java.net.URL;
import java.text.*;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

/**
 *
 * @author hoshino
 */
public class FXMLDocumentController implements Initializable {

	private double left;
	private double right;

	@FXML
	TextField leftNumber;
	@FXML
	TextField rightNumber;
	@FXML
	TextField answer;
	@FXML
	Label operator;
	@FXML
	Button button0;
	@FXML
	Button button1;
	@FXML
	Button button2;
	@FXML
	Button button3;
	@FXML
	Button button4;
	@FXML
	Button button5;
	@FXML
	Button button6;
	@FXML
	Button button7;
	@FXML
	Button button8;
	@FXML
	Button button9;
	@FXML
	Button buttonDot;
	@FXML
	Button buttonMinus;
	@FXML
	Button buttonMultiply;
	@FXML
	Button buttonDivide;
	@FXML
	Button buttonPlus;
	@FXML
	Button buttonEqual;
	@FXML
	Button buttonAC;

	@FXML
	private void handleButtonAction(ActionEvent event) {
		Button button = (Button) event.getSource();
		operator.setText(button.getText());
	}

	@FXML
	private void handleOnNumberButtonClicked(ActionEvent event) {
		TextField textField;
		if (operator.getText().equals("□")) {
			textField = leftNumber;
		} else {
			textField = rightNumber;
		}
		Button button = (Button) event.getSource();
		textField.setText(textField.getText() + button.getText());
	}

	@FXML
	private void handleOnEqualButtonClicked(ActionEvent event) {
		double kotae = 0;
		switch (operator.getText()) {
		case "+":
			kotae = left + right;
			break;
		case "-":
			kotae = left - right;
			break;
		case "×":
			kotae = left * right;
			break;
		case "÷":
			kotae = left / right;
			break;
		default:
			break;
		}
		// System.out.println(kotae);
		String str = (kotae == (int) kotae) ? String.format("%d", (int) kotae) : String.format("%s", kotae);
		answer.setText(str);
	}

	@FXML
	private void handleOnACButtonClicked(ActionEvent event) {
		operator.setText("□");
		left = right = 0;
		leftNumber.setText("");
		rightNumber.setText("");
		answer.setText("");
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		leftNumber.textProperty().addListener((observable, oldValue, newValue) -> {
			left = newValue.equals("") ? 0 : Double.valueOf(newValue);
			// System.out.println(left);
		});

		rightNumber.textProperty().addListener((observable, oldValue, newValue) -> {
			right = newValue.equals("") ? 0 : Double.valueOf(newValue);
			// System.out.println(right);
		});
	}

}
